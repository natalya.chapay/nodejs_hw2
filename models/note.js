const { Schema, model } = require('mongoose');

const noteSchema = Schema({
  completed: {
    type: Boolean,
    required: true,
    default: false,
  },
  text: {
    type: String,
    require: true,
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'user',
  },
}, { versionKey: false, timestamps: { createdAt: 'createdDate', updatedAt: false } });

const Note = model('note', noteSchema);

module.exports = {
  Note,
};
