const express = require('express');

const notes = require('../../controllers/notes');

const { ctrlWrapper } = require('../../helpers');

const { validation, isValidId, user } = require('../../middlewares');

const { add } = require('../../schemas');

const router = express.Router();

router.get('/', user, ctrlWrapper(notes.listNotes));

router.get('/:id', user, isValidId, ctrlWrapper(notes.getById));

router.post('/', user, validation(add), ctrlWrapper(notes.addNote));

router.patch('/:id', user, isValidId, ctrlWrapper(notes.updateCompleted));

router.put('/:id', user, isValidId, ctrlWrapper(notes.updateNote));

router.delete('/:id', user, isValidId, ctrlWrapper(notes.removeNote));

module.exports = router;
