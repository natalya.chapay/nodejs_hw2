const { Note } = require('../../models/note');

const addNote = async (req, res) => {
  const { _id } = req.user;
  const { text } = req.body;
  await Note.create({ text, userId: _id });
  res.status(200).json({
    message: 'Success',
  });
};

module.exports = addNote;
