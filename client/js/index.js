const register = document.querySelector(".register");
const login = document.querySelector(".login");
const logout = document.querySelector(".logout");
const registerSection = document.querySelector(".register-section");
const loginSection = document.querySelector(".login-section");
const notes = document.querySelector(".notes");
const menu = document.querySelector(".menu");
let succsess = false;

let registerForm;
let loginForm;

const token = localStorage.getItem("token") ?? null;

if (!token) {
  notes.hidden = true;
  logout.hidden = true;
  menu.hidden = true;
}

if (token) {
  register.hidden = true;
  login.hidden = true;
}

register.addEventListener("click", onRegisterBtnClick);
login.addEventListener("click", onLoginBtnClick);
logout.addEventListener("click", onLogoutBtnClick);

// ---------------------Register-------------------------

function onRegisterBtnClick() {
  loginSection.innerHTML = "";
  registerSection.innerHTML = `<form action="http://localhost:8080/api/auth/register" class="register-form" method="post">
    <legend class="form-title">Register</legend>
    <label for="username" class="form-label">Username
        <input id="username" type="text" name="username" class="form-input">
    </label>
    <label for="password" class="form-label">Password
        <input id="password" type="text" name="password" class="form-input">
    </label>
    <button type="submit" class="form-btn">Register</button>
</form>`;
  registerForm = document.querySelector(".register-form");
  registerForm.addEventListener("submit", onRegisterFormSubmit);
}

function onRegisterFormSubmit(event) {
  onFormSubmit(event);
  if (!succsess) {
    return;
  }
  onLoginBtnClick();
}

// -------------------------Login----------------------------

function onLoginBtnClick() {
  registerSection.innerHTML = "";
  loginSection.innerHTML = `<form action="http://localhost:8080/api/auth/login" class="login-form" method="post">
    <legend class="form-title">Login</legend>
    <label for="username" class="form-label">Username
        <input id="username" type="text" name="username" class="form-input">
    </label>
    <label for="password" class="form-label">Password
        <input id="password" type="text" name="password" class="form-input">
    </label>
    <button type="submit" class="form-btn">Login</button>
</form>`;
  loginForm = document.querySelector(".login-form");
  loginForm.addEventListener("submit", onLoginFormSubmit);
}

function onLoginFormSubmit(event) {
  onFormSubmit(event);
  if(!succsess) {
    return;
  }
  loginSection.innerHTML = "";
  login.hidden = true;
  register.hidden = true;
  logout.hidden = false;
  notes.hidden = false;
}

// -----------------------Logout---------------------------

function onLogoutBtnClick() {
  localStorage.removeItem("token");
  notes.hidden = true;
  logout.hidden = true;
  menu.hidden = true;
  register.hidden = false;
  login.hidden = false;
}

// --------------------Submit form----------------------------

async function onFormSubmit(event) {
  event.preventDefault();
  const form = event.currentTarget;
  const url = form.action;
  try {
    const formData = new FormData(form);
    await postFormData({ url, formData });
    form.reset();
    succsess = true;
  } catch (error) {
    alert(error);
  }
}

async function postFormData({ url, formData }) {
  const dataObject = Object.fromEntries(formData.entries());
  const formDataString = JSON.stringify(dataObject);

  const fetchOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: formDataString,
  };
  const response = await fetch(url, fetchOptions);

  if (!response.ok) {
    const errorMessage = await response.json();
    return alert(errorMessage.message);
  }

  const result = await response.json();

  if (result.jwt_token) {
    localStorage.setItem("token", result.jwt_token);
  }

  return result;
}
