const { Schema, model } = require('mongoose');

const userSchema = Schema({
  username: {
    type: String,
    required: [true, 'Username is required'],
    unique: true,
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
  },
}, { versionKey: false, timestamps: { createdAt: 'createdDate', updatedAt: false } });

const User = model('user', userSchema);

module.exports = {
  User,
};
